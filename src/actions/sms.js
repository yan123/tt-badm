import { smsConstant as Types } from "./../constants";

function Init() {
    return (dispatch, getState) => {
        const { sms } = getState();
        const storageData = localStorage.getItem("sms_data");
        if(storageData){
            const data = JSON.parse(storageData);
            dispatch({ type: Types.INIT_SMS, table: { ...sms.table, data: { items: data, ready: true } } });
        } else dispatch({ type: Types.INIT_SMS, table: { ...sms.table, data: { items: [], ready: true } } });

    };
}

function Set(data) {
    return (dispatch, getState) => {
        const storageData = JSON.parse(localStorage.getItem("sms_data")) || [];
        if (data.length){
            for(let sms of data){
                storageData.push(sms);
            }
        }

        localStorage.setItem("sms_data", JSON.stringify(storageData));
    };
}

function SetPagination(pageSize) {
    return (dispatch, getState) => {
        const { sms } = getState();
        sms.table.pagination.limit = pageSize;
        dispatch({ type: Types.SET_PAGINATION, table: { ...sms.table } });
    };
}

export { Init, Set, SetPagination };
