import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { compose, combineReducers, createStore, applyMiddleware } from "redux";
import * as serviceWorker from "./serviceWorker";
import thunk from "redux-thunk";
import "./style/index.css";

import App from "./components/App";

import rootReducer from "./reducers";

const store = createStore(combineReducers(rootReducer), compose(applyMiddleware(thunk)));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById("root")
);
serviceWorker.unregister();
