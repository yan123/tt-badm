import React, { Component } from "react";
import ReactTable from "react-table";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Init, SetPagination } from "./../actions/sms";
import moment from "moment";

import "./../style/App.css";

const schemaStatus = ["Отправлено", "Доставлено", "Просмотрено"];
const columns = [
    {
        Header: "Создано",
        id: "created_at",
        accessor: d => {
            return moment(d.created_at)
                .local()
                .format("DD.MM.YYYY HH:mm:ss");
        },
        filterable: false
    },
    {
        Header: "Статус",
        accessor: "status",
        id: "status",
        Cell: props => <span className="gray-background">{schemaStatus[props.value]}</span>,
        filterMethod: (filter, row) => {
            if (filter.value === "all") return true;
            return filter.value === row[filter.id];
        },
        Filter: ({ filter, onChange }) => {
            return (
                <select
                    onChange={event => onChange(event.target.value)}
                    style={{ width: "100%" }}
                    value={filter ? filter.value : "all"}
                >
                    <option value="all">Все</option>
                    <option value="0">Отправлено</option>
                    <option value="1">Доставлено</option>
                    <option value="2">Просмотрено</option>
                </select>
            );
        }
    },
    {
        Header: "Логин",
        accessor: "login",
        filterMethod: (filter, row) => {
            if (row[filter.id].indexOf(filter.value) !== -1) return true;
        }
    },
    {
        Header: "ФИО",
        accessor: "full_name",
        filterMethod: (filter, row) => {
            if (row[filter.id].indexOf(filter.value) !== -1) return true;
        }
    },
    {
        Header: "Дата статуса",
        id: "date_status",
        accessor: d => {
            return moment(d.date_status)
                .local()
                .format("DD.MM.YYYY HH:mm:ss");
        },
        filterable: false
    },
    {
        Header: "Сообщение",
        accessor: "message",
        filterMethod: (filter, row) => {
            if (row[filter.id].indexOf(filter.value) !== -1) return true;
        }
    },
    {
        Header: "Создал",
        accessor: "created_by",
        filterMethod: (filter, row) => {
            if (row[filter.id].indexOf(filter.value) !== -1) return true;
        }
    }
];

class smsNotifocations extends Component {
    constructor() {
        super();
        this.state = {
            filtered: {}
        };
    }
    componentDidMount() {
        this.props.Init();
    }
    test(pageSize, pageIndex) {
        this.props.SetPagination(pageSize);
    }
    render() {
        const { table } = this.props;
        const data = table.data.items;
        const pagination = table.pagination;
        return (
            <div>
                <div className="table-header">
                    <div className="table-header-title">
                        <h1>Уведомления</h1>
                    </div>
                    <div className="table-header-filters" />
                </div>
                <ReactTable
                    filterable
                    previousText="Назад"
                    rowsText="записей"
                    defaultSorted={[
                        {
                            id: "created_at",
                            desc: true
                        }
                    ]}
                    ofText="из"
                    defaultPageSize={pagination.limit}
                    onPageSizeChange={(pageSize, pageIndex) => {
                        this.test(pageSize, pageIndex);
                    }}
                    pageText="Страница"
                    nextText="Далее"
                    data={data}
                    columns={columns}
                />
            </div>
        );
    }
}
const widthConnect = connect(
    state => ({ table: state.sms.table }),
    dispatch => bindActionCreators({ Init, SetPagination }, dispatch)
);

export default widthConnect(smsNotifocations);
