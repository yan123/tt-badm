import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Set } from "./../actions/sms";

import "./../style/App.css";

class createSMS extends Component {
    constructor(props) {
        super(props);
        this.state = { users: [{ login: "", full_name: "" }], message: "" };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeMessage = this.handleChangeMessage.bind(this);
        this.addClick = this.addClick.bind(this);
    }

    createUI() {
        return this.state.users.map((el, i) => (
            <div key={i}>
                <input
                    type="text"
                    name="login"
                    placeholder="Номер телефона"
                    value={el.login || ""}
                    onChange={this.handleChangeLogin.bind(this, i)}
                />
                <input
                    type="text"
                    name="full_name"
                    placeholder="ФИО"
                    value={el.full_name || ""}
                    onChange={this.handleChangeFullName.bind(this, i)}
                />
                <input type="button" value="Удалить" onClick={this.removeClick.bind(this, i)} />
            </div>
        ));
    }

    handleChangeLogin(i, event) {
        let users = [...this.state.users];
        users[i].login = event.target.value;
        this.setState({ users });
    }
    handleChangeFullName(i, event) {
        let users = [...this.state.users];
        users[i].full_name = event.target.value;
        this.setState({ users });
    }
    handleChangeMessage(event) {
        let message =  event.target.value;
        this.setState({ message  });
    }

    addClick() {
        this.setState(prevState => ({ users: [...prevState.users, { login: "", full_name: "" }] }));
    }

    removeClick(i) {
        let users = [...this.state.users];
        users.splice(i, 1);
        this.setState({ users });
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.message.replace(/\s/g, "") !== "") {
            const data = [];
            if(this.state && this.state.users && this.state.users.length) {
                for(let user of this.state.users){
                    data.push({
                        full_name: user.full_name,
                        login: user.login,
                        status: 0,
                        message: this.state.message,
                        created_at: +new Date(),
                        date_status: +new Date(),
                        created_by: "Admin"
                    });
                }
            }
            this.props.Set(data);
            this.setState({ users: [{ login: "", full_name: "" }], message: "" });
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="body-form">
                    {this.createUI()}
                    <div>
                        <textarea
                            placeholder="Сообщение"
                            value={this.state.message}
                            name="value"
                            onChange={this.handleChangeMessage}
                        />
                    </div>
                    <div>
                        <input type="button" value="Добавить клиента" onClick={this.addClick.bind(this)} />
                        <input type="submit" className="btn-send" value="Отправить" />
                    </div>
                </div>
            </form>
        );
    }
}

const widthConnect = connect(state => ({ table: state.sms.table }), dispatch => bindActionCreators({ Set }, dispatch));

export default widthConnect(createSMS);
