import React, { Component } from "react";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";

import smsNotifocations from "./smsNotifocations";
import createSMS from "./createSMS";
import logo from "./../logo.svg";

import "./../style/App.css";

class App extends Component {
    render() {
        return (
            <div>
                <Router>
                    <div className="header-menu">
                        <ul>
                            <li>
                                <img alt="logo" src={logo} />
                            </li>
                            <li>
                                <Link to="/create_sms">Создать смс</Link>
                            </li>
                            <li>
                                <Link to="/">К просмотру смс</Link>
                            </li>
                        </ul>
                    </div>
                    <Route exact path="/" component={smsNotifocations} />
                    <Route exact path="/create_sms" component={createSMS} />
                </Router>
            </div>
        );
    }
}
export default App;
