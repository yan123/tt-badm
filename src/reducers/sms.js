import { smsConstant as Types } from "./../constants";

const defaultState = {
    table: {
        filter: {},
        data: {
            items: [],
            ready: false
        },
        pagination: {
            limit: 10
        }
    }
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case Types.INIT_SMS:
        case Types.SET_SMS:
        case Types.SET_PAGINATION:
            return { ...state, table: action.table };
        default:
            return state;
    }
};
